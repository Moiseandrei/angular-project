import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class MovieManagerService {
  private movies: Array<any> = []
  private idKey: number = 1;

  constructor(private http: HttpClient) {
    //   this.movies.push({
    //     id: this.getIdKey(),
    //     title: `${this.movies.length} - Morometii`,
    //
    //     // linia de sus este echivalentul lui ( this.movie.length + " - morometii " )
    //
    //     year: 2008.,
    //     description: "Description",
    //     director: "Director",
    //   })
    //   this.movies.push({
    //     id: this.getIdKey(),
    //     title: `${this.movies.length} - Pacala`,
    //     year: 2008.,
    //     description: "Description",
    //     director: "Director",
    //   })
    //   this.movies.push({
    //     id: this.getIdKey(),
    //     title: `${this.movies.length} - Dracula`,
    //     year: 2008.,
    //     description: "Description",
    //     director: "Director",
    //   })
  }

  add(movie: any): any {
    // this.movies.push(movie);
    // return this.http.post(`${environment.baseUrl}/api/movie`, movie);
    let body = {
      id: movie.id,
      title: movie.title,
      description: movie.description,
      year: movie.year,
      director: movie.director,
    }
    return this.http.post(`${environment.baseUrl}/api/movie`, body);
  }

  update(movie: any): any {
    let body = {
      title: movie.title,
      description: movie.description,
      year: movie.year,
      director: movie.director,
    }
    return this.http.patch(`${environment.baseUrl}/api/movie/${movie.id}`, body);

  }

  delete(movie: any): any {
    //this.movies = this.movies.filter((item) => item.id != movie.id);
    return this.http.delete(`${environment.baseUrl}/api/movie/${movie.id}`);
  }

  get(): any {
    //return this.movies;
    return this.http.get(`${environment.baseUrl}/api/movie`);
  }

  getById(id: number): any {
    // let items = this.movies.filter((item) => item.id == id);
    // if (items.length == 1) {
    //   return items[0];
    // } else {
    //   return null;
    // }
    return this.http.get(`${environment.baseUrl}/api/movie/${id}`);
  }

  getIdKey(): number {
    return this.idKey += 1;
  }
}
